# Multidimensional Scaling

In the docs folder are papers which describe the methods.  
[AuswertungSJT.pdf](./docs/Auswertung%20SJT.pdf) describes what is needed to be calculated.  
Assets include example data and regarding plots.

## Useful Links

- [Wikipedia MDS](https://en.wikipedia.org/wiki/Multidimensional_scaling)
- [Data Visualization using Multidimensional Scaling](https://www.benfrederickson.com/multidimensional-scaling/) with JavaScript
  - [Used MDS Lib](https://github.com/benfred/mds.js)
- [Multidimensional Scaling](https://www.statmethods.net/advstats/mds.html) with R

## Tools used in Papers

- [PINDIS](http://apb.newmdsx.com/pindis.html)
